﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public int PlayerHealth;
    public int EnemyHealth;
    public void Awake()
    {
        PlayerHealth = 100;
        EnemyHealth = 100;
    }
}
