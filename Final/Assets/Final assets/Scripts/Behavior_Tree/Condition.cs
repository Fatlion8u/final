﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Condition
{
    public virtual bool Test()
    {
        return false;
    }

    public virtual bool PlayerAlive()
    {
        if (GameObject.FindGameObjectWithTag("Player").GetComponent<Health>().PlayerHealth >= 1)
        {
            return true;
        }

        return false;
    }
    public virtual bool EnemyAlive()
    {
       if(GameObject.FindObjectOfType<Enemy>().GetComponent<Health>().EnemyHealth >=1 )
            return true;
        return false;
    }

    public virtual bool Dead()
    {
        if(GameObject.FindObjectOfType<Enemy>().GetComponent<Health>().EnemyHealth <= 0)
            return true;
        return false;
    }

}
