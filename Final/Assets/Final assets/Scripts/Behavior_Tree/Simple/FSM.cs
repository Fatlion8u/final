﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityScript.Scripting.Pipeline;
using UnityScript.Steps;

public class FSM : FSM_Master {
    public enum FSMstate
    {
        None,
        Patrol,
        Chase,
        Attack,
        Dead,
    }

    public FSMstate CurrentState;
    private float CurrentSpeed;
    private float CurrentRotSpeed;
    private bool bDead;
    private int health;
    public GameObject Bullet;

    public int Health(){
        return health;
    }
    protected override void Initialize()
    {
        CurrentState = FSMstate.Patrol;
        CurrentSpeed = 150f;
        CurrentRotSpeed = 2.0f;
        bDead = false;
        elapsedTime = 0f;
        health = 100;
        shootRate = 3f;
        PointList = GameObject.FindGameObjectsWithTag("Point");
        FindNextPoint();
        GameObject objPlayer = GameObject.FindGameObjectWithTag("Player");
        PlayerTransform = objPlayer.transform;
        if (!PlayerTransform)
        {
            print("Player does not exist");
        }

        turret = gameObject.transform.GetChild(0).transform;
        BSpawn = turret.GetChild(0).transform;
    }

    protected override void FSMUpdate()
    {
        switch (CurrentState)
        {
            case FSMstate.Patrol:
                UpdatePatrolState();
                break;
            case FSMstate.Chase:
                UpdateChaseState();
                break;
            case FSMstate.Attack:
                UpdateAttackState();
                break;
            case FSMstate.Dead:
                UpdateDeadState();
                break;
        }

        elapsedTime += Time.deltaTime;
        if (health <= 0)
        {
            CurrentState = FSMstate.Dead;
        }
    }

    protected void UpdatePatrolState()
    {
        if (Vector3.Distance(transform.position, DestinationPos) <= 100f)
        {
            FindNextPoint();
        }
        else if (Vector3.Distance(transform.position, PlayerTransform.position) <= 300f)
        {
            CurrentState = FSMstate.Chase;
        }
    }

    protected void FindNextPoint()
    {
        int rndIndex = Random.Range(0, PointList.Length);
        float rndRadius = 10f;
        Vector3 rndPosition = Vector3.zero;
        DestinationPos = PointList[rndIndex].transform.position + rndPosition;
        if (IsInCurrentRange(DestinationPos))
        {
            rndPosition = new Vector3(Random.Range(-rndRadius, rndRadius), 0f, Random.Range(-rndRadius, rndRadius));
            DestinationPos = PointList[rndIndex].transform.position + rndPosition;
        }
    }

    protected bool IsInCurrentRange(Vector3 pos)
    {
        float xPos = Mathf.Abs(pos.x - transform.position.x);
        float zPos = Mathf.Abs(pos.z - transform.position.z);
        if (xPos <= 50 && zPos <= 50)
            return true;
        return false;
    }

    protected void UpdateChaseState()
    {
        DestinationPos = PlayerTransform.position;

        float dist = Vector3.Distance(transform.position,
            PlayerTransform.position);

        if (dist <= 200.0f)
        {
            CurrentState = FSMstate.Attack;
        }

        else if (dist >= 300.0f)
        {
            CurrentState = FSMstate.Patrol;
        }

        transform.Translate(Vector3.forward * Time.deltaTime * CurrentSpeed);
    }

    protected void UpdateAttackState()
    { 
            DestinationPos = PlayerTransform.position;

        float dist = Vector3.Distance(transform.position, PlayerTransform.position);

        if (dist >= 200.0f && dist < 300.0f)
        {
            Quaternion targetRotation =
                Quaternion.LookRotation(DestinationPos - transform.position);
            transform.rotation = 
                Quaternion.Slerp( transform.rotation, targetRotation, Time.deltaTime * CurrentRotSpeed);

            transform.Translate(Vector3.forward * Time.deltaTime * CurrentSpeed);

            CurrentState = FSMstate.Attack;
        }
        else if (dist >= 300.0f)
        {
            CurrentState = FSMstate.Patrol;
        }

        Quaternion turretRotation =
            Quaternion.LookRotation(DestinationPos - turret.position);

        turret.rotation =
            Quaternion.Slerp(turret.rotation, turretRotation, Time.deltaTime * CurrentRotSpeed);

        ShootBullet();
    }
    private void ShootBullet()
    {
        if (elapsedTime >= shootRate)
        {
            Instantiate(Bullet, BSpawn.position, BSpawn.rotation);
            elapsedTime = 0.0f;
        }
    }

    protected void UpdateDeadState()
    {
        if (!bDead)
        {
            bDead = true;
            Explode();
        }
    }

    protected void Explode()
    {
        float rndX = Random.Range(10f, 30f);
        float rndZ = Random.Range(10f, 30f);
        for (int i = 0; i < 3; i++)
        {
            GetComponent<Rigidbody>().AddExplosionForce(100f,transform.position- new Vector3(rndX,10f,rndZ),40f,10f);
            GetComponent<Rigidbody>().velocity = transform.TransformDirection(new Vector3(rndX, 20f, rndZ));
        }
        this.gameObject.SetActive(false);
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            health -= other.gameObject.GetComponent<Bullet>().damage;
        }
    }
}

