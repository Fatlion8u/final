﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSM_Master : MonoBehaviour
{

    protected Transform PlayerTransform;
    protected Vector3 DestinationPos;
    protected GameObject[] PointList;
    protected float shootRate;
    protected float elapsedTime;
    public Transform turret { get; set; }
    public Transform BSpawn { get; set; }

    protected virtual void Initialize() {}

    protected virtual void FSMUpdate() {}
    protected virtual void FSMFixedUpdate() { }

    void Start()
    {
        Initialize();
    }

    void Update()
    {
        FSMUpdate();
    }

    void FixedUpdate()
    {
        FSMFixedUpdate();
    }
}
