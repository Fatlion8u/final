﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State : MonoBehaviour
{
    public List<Transition> transitions;
    public virtual void Awake()
    {
        transitions = new List<Transition>();
        //To-do
        //setup transitions
        
    }

    public virtual void OnEnable()
    {
        //develop state init
        
    }

    public virtual void OnDisable()
    {
        //state finals
    }

    public virtual void update()
    {
        //TODO
        //develop behavior here
        
    }

    public void LateUpdate()
    {
        foreach (Transition t in transitions)
        {
            if (t.condition.Test())
            {
                t.target.enabled = true;
                this.enabled = false;
                return;
            }

            if (t.condition.EnemyAlive())
            {
                t.Idle.enabled = true;
                this.enabled = false;
                return;
            }

            if (t.condition.Dead())
            {
                t.Idle.enabled = true;
                this.enabled = false;
                GameObject.Destroy(this,10f);
                return;
            }
        }
    }
}
