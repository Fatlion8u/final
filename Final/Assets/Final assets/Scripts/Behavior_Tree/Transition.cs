﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transition
{
    public Condition condition;
    public State target;

    public State Idle;
    public State Curious;
    public State Afraid;
    public State Patrol;
    public State Dead;
}
