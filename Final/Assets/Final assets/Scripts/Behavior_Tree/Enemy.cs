﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityScript.Macros;

//using UnityEngine.Animations; // not needed
public class Enemy : MonoBehaviour
{
    private Vector3 lastPlayerPosition;
    private Animator animator;
    private Rigidbody rigidbody;
    private State CurrentState;
    public GameObject Player;

    public float minSafeRange = 5f;

    public float maxSafeRange = 10f;


    // Use this for initialization
    void Start ()
	{
	    CurrentState = State.STATE_IDLE;
	    rigidbody = GetComponent<Rigidbody>();
	    animator = GetComponent<Animator>();
        Player = GameObject.FindGameObjectWithTag("Player");
	}

    void OnCollisionEnter(Collision col) {
        if (col.gameObject.CompareTag("Player"))
        {
            animator.SetTrigger("Move_Away");
            Vector3 newVelocity = -(rigidbody.velocity);
            rigidbody.velocity = newVelocity;
        }

        lastPlayerPosition = Player.transform.position;
    }
	// Update is called once per frame
	void Update ()
	{
	    if (Player.transform.position != lastPlayerPosition)
	    {
            Transition();
	    }
	}

	// little excerpt from your first week
   

    enum State
    {
        STATE_IDLE,
        STATE_CURIOUS,
        STATE_AFRAID,
        STATE_Patrol
    }

    void Transition()
    {
        Vector3 distance = Player.transform.position - gameObject.transform.position;
        switch (CurrentState)
        {
            case State.STATE_IDLE:
                Debug.Log("Idle");
                
                if (distance.magnitude < minSafeRange)
                {
                    rigidbody.AddForce(distance.normalized);
                }
                else if (distance.magnitude > maxSafeRange)
                {
                    CurrentState = State.STATE_CURIOUS;
                }
                break;
            case State.STATE_CURIOUS:
                rigidbody.AddForce(distance.normalized *20);
                if ((distance.magnitude >= minSafeRange) && (distance.magnitude <= maxSafeRange))
                {
                    CurrentState = State.STATE_IDLE;
                }
                Debug.Log("Curious");
                break;
            case State.STATE_AFRAID:
                rigidbody.AddForce(-(distance.normalized)*20);
                if ((distance.magnitude >= minSafeRange) && (distance.magnitude <= maxSafeRange))
                {
                    CurrentState = State.STATE_IDLE;
                }
                break;
            case State.STATE_Patrol:
                break;
        }
    }
}
