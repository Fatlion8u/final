﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateHighLevel : State
{
    public List<State> states;
    public State stateinit;
    protected State currentState;

    public override void OnEnable()
    {
        if (currentState == null)
        {
            currentState = stateinit;
        }

        currentState.enabled = true;
    }

    public override void OnDisable()
    {
        base.OnDisable();
        currentState.enabled = false;
        foreach (State s in states)
        {
            s.enabled = false;
        }
    }
}
