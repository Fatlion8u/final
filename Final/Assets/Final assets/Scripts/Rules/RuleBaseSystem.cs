﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RuleBaseSystem : MonoBehaviour {
    public GameObject player;
    private Vector3 lastPlayerPosition;
    private Rigidbody rigidbody;
    private ParticleSystem particles;
    private ParticleSystem.MainModule partMain;
    // Use this for initialization
    void Start () {
        rigidbody = GetComponent<Rigidbody>();
        particles = GetComponent<ParticleSystem>();
        partMain = particles.main;
    }
	
	// Update is called once per frame
	void Update () {
	    if (player.transform.position != lastPlayerPosition)
	    {
	        ProcessRules();
	    }
	    lastPlayerPosition = player.transform.position;
    }
    void ProcessRules()
    {
        Vector3 distance = transform.position - player.transform.position;

        // if the player is too close, jump
        if (distance.magnitude <= 2)
        {
            particles.Play();
            rigidbody.AddForce(0.0f, 10.0f, 0.0f);
        }
        else
        {
            //particles.Stop();
        }
        // if the object is above a certain height, change color
        if (transform.position.y > 2.0f)
        {
            partMain.startColor = Color.blue;
        }
        else
        {
            partMain.startColor = Color.red;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Wall"))
        {
            Vector3 newVelocity = -(rigidbody.velocity);
            rigidbody.velocity = newVelocity;
        }
    }
}
