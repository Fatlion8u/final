﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public class fuzzy : MonoBehaviour {
    public GameObject player;
    private Rigidbody rigidBody;
    public AnimationCurve fuzzyLogicCurve;
    public float activationLevel = 0.8f;
    // Use this for initialization
    void Start ()
    {
        rigidBody = GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame


    void ProcessFuzzy()
    {
        Vector3 distance = transform.position - player.transform.position;
        float activedLevel = (distance.magnitude * distance.magnitude) / 100;
        if (fuzzyLogicCurve.Evaluate(distance.magnitude) > activationLevel)
        {
            rigidBody.AddTorque(new Vector3(0f,100f * activationLevel,0f));
        }
    }
    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            ProcessFuzzy();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Wall"))
        {
            Vector3 newVelocity = -(rigidBody.velocity);
            rigidBody.velocity = newVelocity;
        }
    }
}
