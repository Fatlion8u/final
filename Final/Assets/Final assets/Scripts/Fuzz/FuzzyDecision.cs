﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuzzyDecision : MonoBehaviour {
    //Dom - Deals of membership
    public Dictionary<int, float> MakeDecision(object[] inputs, MembershipFunction[][] mflist, FuzzyRule[] rules)
    {
        Dictionary<int, float> inputDOM = new Dictionary<int, float>();
        Dictionary<int, float> OutputDOM = new Dictionary<int, float>();
        MembershipFunction memberfunc;
        //
        foreach (object input in inputs)
        {
            int r, c;
            for (r = 0; r < mflist[r].Length; r++)
            {
                for (c = 0; c < mflist[r].Length; c++)
                {
                    //TODO
                    memberfunc = mflist[r][c];
                    int mfld = memberfunc.stateid;
                    float dom = memberfunc.GETDOM(input);
                    if (!inputDOM.ContainsKey(mfld))
                    {
                        inputDOM.Add(mfld, dom);
                        OutputDOM.Add(mfld, 0f);
                    }
                    else
                    {
                        inputDOM[mfld] = dom;
                    }
                }
            }


        }

        foreach (FuzzyRule rule in rules)
        {
            int outputid = rule.conclusionStateid;
            float best = OutputDOM[outputid];
            float min = 1f;
            foreach (int state in rule.stateids )
            {
                float dom = inputDOM[state];
                if (dom < best)
                {
                    continue;
                }

                if (dom < min)
                {
                    min = dom;
                }
            }

            OutputDOM[outputid] = min;
        }

        return OutputDOM;
    }
}