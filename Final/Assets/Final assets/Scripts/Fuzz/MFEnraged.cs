﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MFEnraged : MembershipFunction
{
    public GameObject _Enemy;
    public GameObject Player;
    private Rigidbody rigidbody;
    public void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
    }
    public override float GETDOM(object input)
    {
        Vector3 distance = Player.transform.position - _Enemy.transform.position;
        if ((int) input <= 30)
        {
            rigidbody.AddForce(-distance);
            return 1f;

        }

        return 0f;
    }
}
