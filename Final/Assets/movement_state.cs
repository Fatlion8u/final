﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement_state : StateMachineBehaviour {
    public GameObject Current;
    public GameObject Player;
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");

    }
    void Move_to()
    {
        Current.transform.position =
            Vector3.Slerp(Current.transform.position, Player.transform.position, Time.deltaTime);
    }

    void move_Away()
    {
        Current.transform.position =
            Vector3.Slerp(Current.transform.position, Vector3.back, Time.deltaTime);
    }
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	    Move_to();
	}
 
	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	    if (GameObject.FindGameObjectsWithTag("Enemy").Length <= 4)
	    {
            move_Away();
            
	    }
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	//override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}



}
